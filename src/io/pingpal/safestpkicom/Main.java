package io.pingpal.safestpkicom;

import com.google.gson.Gson;
import com.squareup.okhttp.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

public class Main {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    static OkHttpClient client;
    public static void main(String[] args) {


        client = new OkHttpClient();
        HashMap<String, String> hMap = new HashMap<String, String>();
        hMap.put("csr","-----BEGIN CERTIFICATE REQUEST-----\n" +
                "MIICmzCCAYMCAQAwVjELMAkGA1UEBhMCc2UxCzAJBgNVBAgMAnNlMQswCQYDVQQH\n" +
                "DAJzZTELMAkGA1UECgwCc2UxCzAJBgNVBAsMAnNlMRMwEQYDVQQDDApkc2Znc2Zn\n" +
                "YXNoMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2jPHnbnqG9JjGpIX\n" +
                "6F/V7UH85z1U+yiX+WsBwIOZtmpm/R6HEHm0QKnKZzo7nvM4SrA1tLmQx8JZcG82\n" +
                "22QixN52Vm96hvrXNOAvqeom9nuS83GawmVkK5KGAVc2Ywu7zfWYzXIAEs1bSe8T\n" +
                "KvwEmzytb9a4XGz+V/RAC/SyetIQzrQRQPt7/JJolJMM5RxaHjfzrSYw47KeMvsn\n" +
                "gmAZSQ2kE8GfUjEdt7Pnaln51F0sHxagTQGiHtc7DoC/lb/M8OppYOBM5WitpCDy\n" +
                "ox4x4yCYMiX9l+hXHPz0kMpsZOVKBV5xIxd6qWSUidQiC82oYTAEdjS0YT2RcOeZ\n" +
                "4jXWkwIDAQABoAAwDQYJKoZIhvcNAQELBQADggEBAHJgX1Qyf7Rq1WiS3Rvv5cc5\n" +
                "De3CT04QhsefbM5Z419zX5uKt22EikRJyCUfrS8ediyGWH0dWhIzWfoLUcvamNIS\n" +
                "dcjdU2/Cmy0EmLiR+ZpZvG9zIXRpeOKiGyqF5VsnEFYTqfuV6VjN7PW4L8gM5CYo\n" +
                "Uy7o0WFvb1HiuOPz7cnOaG48QhFnfyL/f3C5v2qvbkQaNcqVg5zJBmin8a1xhCO8\n" +
                "PlOLn99XjynJpZZeOd5iofxl9FZQXQEbrklZ8q0awyltz9O3Gd7iHZetHnMVVM/u\n" +
                "Xg2hpNzOHG+kGojYww7NhWeUFUd2UX8syvGQm9++/umVCGaEV+VndEU3udJz65w=\n" +
                "-----END CERTIFICATE REQUEST-----\n");
        String json = Main.toJson(hMap);
        try {
            String answer = post("http://pkiserver:8080/addusr", json);
            System.out.println("Answer: " + answer);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    static String toJson(Object pojo){
        Gson gson = new Gson();
        return gson.toJson(pojo);
    }

    static String post(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}
